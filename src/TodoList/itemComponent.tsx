import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import Checkbox from "@material-ui/core/Checkbox";
import ListItemText from "@material-ui/core/ListItemText";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import IconButton from "@material-ui/core/IconButton";
import {Delete} from "@material-ui/icons";
import React from "react";

export type ItemData = {
    id: string,
    text: string,
    isNoteToggle: boolean,
}

export type ListItemProps = {
    item: ItemData,
    handleToggle: (id: string) => void,
    deleteItemFunc: (id: string) => void,
    hasOnClickHandle: boolean,
    onClickItemHandler: (itemClicked: ItemData) => void
    //onClickItem?: (itemProps: ListItemProps) => void,
}

export function ItemComponent(propsItem: ListItemProps) {

    // onClick={() => propsItem.onClickItem?.(propsItem)}
    const onClickItemHandle = () => {
        if (propsItem.hasOnClickHandle) {
            propsItem.onClickItemHandler(propsItem.item);
        }
    };
    return (
        <div>
            <ListItem dense button>
                <ListItemIcon>
                    <Checkbox
                        edge="start"
                        checked={propsItem.item.isNoteToggle}
                        onClick={() => propsItem.handleToggle(propsItem.item.id)}
                    />
                </ListItemIcon>
                <ListItemText className={propsItem.item.isNoteToggle ? 'Strike' : ''} primary={propsItem.item.text}
                              onClick={onClickItemHandle}/>
                <ListItemSecondaryAction>
                    <IconButton edge="end" onClick={() => propsItem.deleteItemFunc(propsItem.item.id)}>
                        <Delete/>
                    </IconButton>
                </ListItemSecondaryAction>
            </ListItem>
        </div>
    );
}

export function findIndexItemById(id: string, list: ItemData[]): number {
    return list.findIndex(value => value.id === id);
}