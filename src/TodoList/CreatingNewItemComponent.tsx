import React, {useState} from "react";
import IconButton from "@material-ui/core/IconButton";
import {Add} from "@material-ui/icons";

type NewItemProps = {
    createItemFunc: (text: string) => void;
}

export const getUniqueId = () => {
    let uniqid = require('uniqid');
    return uniqid.time();
};

export function CreateNewItemComponent(newItemProps: NewItemProps) {
    const [text, setText] = useState('');

    const handleChange = (event: React.FormEvent<HTMLInputElement>) => {
        setText(event.currentTarget.value);
    };

    const addItem = () => {
        if (text.trim() !== '') {
            newItemProps.createItemFunc(text);
            setText('');
        }
    };

    return (
        <div>
            <input type="text" placeholder="text" value={text} onChange={handleChange}/>
            <IconButton edge="end" onClick={addItem}>
                <Add/>
            </IconButton>
        </div>
    );
}