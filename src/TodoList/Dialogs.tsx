import React from "react";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import List from "@material-ui/core/List";
import DialogActions from "@material-ui/core/DialogActions";
import Button from "@material-ui/core/Button";
import {ItemComponent, ItemData, ListItemProps, findIndexItemById} from "./itemComponent";


type dialogProps = {
    isDialogOpen: boolean,
    listItemProps: ListItemProps,
    listItems: ItemData[],
    onCloseHandle: () => void,
}

export function ListItemDialog(dialogProp: dialogProps) {
    console.log("inDialog");
    let item : ItemData = dialogProp.listItems[findIndexItemById(dialogProp.listItemProps.item.id, dialogProp.listItems)];


    const onCloseHandle = () => {
        dialogProp.onCloseHandle();
    };

    if(item !== undefined) {
        return (
            <Dialog open={dialogProp.isDialogOpen} onClose={onCloseHandle} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Item Full Details</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        You can edit the item by pressing
                    </DialogContentText>
                    <List>
                        <ItemComponent item={item}
                                       handleToggle={dialogProp.listItemProps.handleToggle}
                                       deleteItemFunc={dialogProp.listItemProps.deleteItemFunc}
                                       hasOnClickHandle={false}
                                       onClickItemHandler={dialogProp.listItemProps.onClickItemHandler}/>

                        {/*<ItemComponent item={itemProps.item}*/}
                        {/*               handleToggle={itemProps.handleToggle}*/}
                        {/*               deleteItemFunc={itemProps.deleteItemFunc}*/}
                        {/*               hasOnClickHandle={false}*/}
                        {/*               onClickItemHandler={itemProps.onClickItemHandler}/>*/}


                        {/*<ItemComponent item={dialogProp.listItems[findIndexItemById(dialogProp.listItemProps.item.id, dialogProp.listItems)]}*/}
                        {/*               handleToggle={dialogProp.listItemProps.handleToggle}*/}
                        {/*               deleteItemFunc={dialogProp.listItemProps.deleteItemFunc}*/}
                        {/*               hasOnClickHandle={false}*/}
                        {/*               onClickItemHandler={dialogProp.listItemProps.onClickItemHandler}/>*/}

                        {/*<ItemComponent item={dialogProp.listItemProps.item} handleToggle={dialogProp.listItemProps.handleToggle}*/}
                        {/*               deleteItemFunc={dialogProp.listItemProps.deleteItemFunc} hasOnClickHandle={false}*/}
                        {/*               onClickItemHandler={dialogProp.listItemProps.onClickItemHandler}/>*/}
                    </List>
                </DialogContent>
                <DialogActions>
                    <Button onClick={onCloseHandle} color="primary">
                        Cancel
                    </Button>
                    <Button onClick={onCloseHandle} color="primary">
                        Save
                    </Button>
                </DialogActions>
            </Dialog>
        );
    }
    else {
        return <div></div>
    }
}