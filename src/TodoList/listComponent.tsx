import React, {useEffect, useState} from "react";
import {createStyles, makeStyles} from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import Button from '@material-ui/core/Button';
import SaveIcon from '@material-ui/icons/Save';
import {ListItemDialog} from "./Dialogs";
import {getAllItemList, saveList} from './axiosRequests';
import {findIndexItemById, ItemComponent, ItemData, ListItemProps} from "./itemComponent";
import {CreateNewItemComponent, getUniqueId} from "./CreatingNewItemComponent";
import {SearchBoxComponent, useFilterSearchList} from "./searchingComponent";

const useStyles = makeStyles(() =>
    createStyles({
        root: {
            width: '100%',
            maxWidth: 460,
            background: 'white'
        },
    }),
);

export default function ListComponent() {
    const classes = useStyles();

    const [allItemsData, setAllItemsData] = useState<ItemData[]>([]);
    useEffect(() => {
        const getItemList = async () => {
            const result = await getAllItemList();
            setAllItemsData(result);
        };
        getItemList();
    }, []);

    const [searchText, setSearchText] = useState<string>('');
    const listItemToShow = useFilterSearchList(searchText, allItemsData);
    const [isDialogOpen, setIsDialogOpen] = useState<boolean>(false);
    const [itemClicked, setItemClicked] = useState<ItemData>({isNoteToggle: false, id: ' ', text: ''});

    let listItemClickedProps: ListItemProps = {
        item: itemClicked,
        hasOnClickHandle: true,
        handleToggle: handleToggle,
        deleteItemFunc: handleDeleteItem,
        onClickItemHandler: openDialogHandler
    };


    function handleToggle(id: string): void {
        console.log("in handleToggle func");
        let newAllItemsDataListFromServer = allItemsData.slice();
        console.log(newAllItemsDataListFromServer);

        let indexForServerList = findIndexItemById(id, newAllItemsDataListFromServer);

        newAllItemsDataListFromServer[indexForServerList] = {
            text: newAllItemsDataListFromServer[indexForServerList].text,
            id: id,
            isNoteToggle: !newAllItemsDataListFromServer[indexForServerList].isNoteToggle
        };


        setAllItemsData(newAllItemsDataListFromServer);
    }

    function handleDeleteItem(id: string): void {
        console.log("in deleteItem func");
        let newAllItemsDataListFromServer = allItemsData.slice().filter(value => value.id !== id);

        setAllItemsData(newAllItemsDataListFromServer);
        setIsDialogOpen(false);
    }

    function searchHandle(searchTerm: string): void {
        setSearchText(searchTerm);
    }

    function addItemToList(text: string) {
        let itemToAdd: ItemData = {
            text: text,
            id: getUniqueId(),
            isNoteToggle: false
        };

        let newAllItemsDataListFromServer = allItemsData.slice();
        newAllItemsDataListFromServer.push(itemToAdd);
        setAllItemsData(newAllItemsDataListFromServer);
    }

    function openDialogHandler(itemClicked: ItemData): void {
        setItemClicked(itemClicked);
        setIsDialogOpen(true)
    }

    function closeDialogHandler(): void {
        setIsDialogOpen(false);
    }

    return (
        <div>
            <SearchBoxComponent searchFunc={searchHandle}/>
            <List className={classes.root}>
                {listItemToShow.map((item: ItemData) => <ItemComponent item={item} handleToggle={handleToggle}
                                                                       deleteItemFunc={handleDeleteItem}
                                                                       hasOnClickHandle={true}
                                                                       onClickItemHandler={openDialogHandler}/>)}
            </List>
            <CreateNewItemComponent createItemFunc={addItemToList}/>
            <Button variant="contained" color="secondary" size="small" onClick={() => saveList(allItemsData)}
                    startIcon={<SaveIcon/>}>
                Save
            </Button>
            <ListItemDialog isDialogOpen={isDialogOpen} listItemProps={listItemClickedProps} listItems={allItemsData} onCloseHandle={closeDialogHandler}/>
        </div>
    );
}

