import React, {useEffect, useState} from "react";
import {ItemData} from "./itemComponent";

type SearchProps = {
    searchFunc: (searchTerm: string) => void;
}

export function SearchBoxComponent(searchProps: SearchProps) {
    const [searchTerm, setSearchTerm] = useState("");
    const handleChange = (event: React.FormEvent<HTMLInputElement>) => {
        setSearchTerm(event.currentTarget.value);
        searchProps.searchFunc(event.currentTarget.value);
    };

    return (
        <div>
            <input type="text" placeholder="Search" value={searchTerm} onChange={handleChange}
            />
        </div>
    );
}

export function useFilterSearchList(searchTerm: string, listItemToFilter: ItemData[]) {
    const [listItem, setListItem] = useState(listItemToFilter);

    useEffect(() => {
        setListItem(listItemToFilter.slice().filter((value) => value.text.toLowerCase().includes(searchTerm.toLowerCase())));
    }, [searchTerm, listItemToFilter]);

    return listItem;
}