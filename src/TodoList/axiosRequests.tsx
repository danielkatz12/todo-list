import axios from 'axios';
import {ItemData} from './itemComponent';

export function getAllItemList(): Promise<ItemData[]> {
    let list: Promise<ItemData[]> =
    axios.get<ItemData[]>('http://localhost:3001/getData')
        .then(response => {
            return response.data;
        })
        .catch(reason =>{
            console.log("getAllItemList request failed!!!- " + reason);
            return [];
        });
    return  list;
}
export function saveList(listToSave: ItemData[]): void {
  axios.post<ItemData[]>('http://localhost:3001/saveData', JSON.stringify(listToSave),{
      headers: {
        'Access-Control-Allow-Origin': '*',
            'Content-Type': 'application/json'
    },
    params: JSON.stringify(listToSave),
        data: JSON.stringify(listToSave)
})
      .then(value => {console.log('List saved SUCCESSFULLY :)'); alert('List saved SUCCESSFULLY :)');})
      .catch(reason => {console.log("saveList request failed!- " + reason); alert('saveList request failed!');});
}
