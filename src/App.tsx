import React from 'react';
import './App.css';
import ListComponent from './TodoList/listComponent'

function App() {
  return (
    <div className="App">
      <ListComponent/>
    </div>
  );
}

export default App;
